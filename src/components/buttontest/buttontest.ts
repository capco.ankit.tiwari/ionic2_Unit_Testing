import { Component } from '@angular/core';

/**
 * Generated class for the ButtontestComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'buttontest',
  templateUrl: 'buttontest.html'
})
export class ButtontestComponent {

  text: string;

  constructor() {
    console.log('Hello ButtontestComponent Component');
    this.text = 'Hello World';
  }

  buttonclick(){
    console.log("Button clicked");
  }

}
