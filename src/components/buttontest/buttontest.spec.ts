import { async, TestBed } from '@angular/core/testing';
import { IonicModule } from 'ionic-angular';
import { ButtontestComponent } from './buttontest';




describe('Buttontest Component', () => {
    let fixture;
    let component;
  
    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [ButtontestComponent],
        imports: [
          IonicModule.forRoot(ButtontestComponent)
        ],
        providers: [
       
        ]
      }).compileComponents
    }));
  
    beforeEach(() => {
      fixture = TestBed.createComponent(ButtontestComponent);
      component = fixture.componentInstance;
    });
  //Check for component created or not
    it('should be created', () => {
      expect(component instanceof ButtontestComponent).toBe(true);
    });

        //button is clicked or not , fuction is called or not
        it('should call function Buttonclick function()', () =>{
   
            spyOn(component, "buttonclick").and.callThrough();
            component.buttonclick();
            expect(component.buttonclick).toHaveBeenCalled();
            });
 
  });