import { NgModule } from '@angular/core';
import { ButtontestComponent } from './buttontest/buttontest';
@NgModule({
	declarations: [ButtontestComponent],
	imports: [],
	exports: [ButtontestComponent]
})
export class ComponentsModule {}
