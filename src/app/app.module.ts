import { ErrorHandler, NgModule  } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import {HttpModule} from  '@angular/http';

import {ProductPage} from '../pages/product/product';
import { Products } from '../providers/products/products'
import { PipesModule } from './../pipes/pipes.module';

import { MyApp } from './app.component';




@NgModule({
  declarations: [
    MyApp
    
   
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    PipesModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Products
  ]
})
export class AppModule { }
