import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-page2',
  templateUrl: 'page2.html'
})
export class Page2 {
 
user:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.user= navParams.get('user');
  }

 
}