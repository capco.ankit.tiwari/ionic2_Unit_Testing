import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { Page2 } from './Page2';
import { IonicModule, Platform, NavController, NavParams} from 'ionic-angular/index';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NavMock,MockNavParams  } from '../../../test-config/mocks-ionic';
import { ButtontestComponent } from '../../components/buttontest/buttontest';
import { ShortArrayPipe } from '../../pipes/short-array/short-array';

describe('Page2', () => {
  let de: DebugElement;
  let comp: Page2;
  let el: HTMLElement;
  let fixture: ComponentFixture<Page2>;
  let comp1: ButtontestComponent;
  let fixture1: ComponentFixture<ButtontestComponent>;
  let pipe;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Page2,ButtontestComponent],
      imports: [
        IonicModule.forRoot(Page2)
      ],
      providers: [
        {
            provide: NavController,
            useClass: NavMock,
            
        },
        {
            provide: NavParams,
            useClass: MockNavParams,
            
        }
    ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Page2);
    comp = fixture.componentInstance;
    fixture1 = TestBed.createComponent(ButtontestComponent);
    comp1 = fixture1.componentInstance;
    de = fixture.debugElement.query(By.css('h3'));
    de = fixture.debugElement.query(By.css('.add-paste'));
    el = de.nativeElement;
     pipe = new ShortArrayPipe();
  });

  it('should create component', () => expect(comp).toBeDefined());

// integration testing for component
  it('should integrate button component', () => expect(comp1).toBeDefined());
  
  
  // Navparams testing , refer navmocks
  it('Should have one device if is received by navparams', () => {
   let result = comp.user

    expect(result._id).toBe("001");
  });



  it('should display the `create Paste` button', () => {
    //There should a create button in view
    fixture.detectChanges();
    const el = fixture.debugElement.query(By.css('buttontest')).nativeElement;
     expect(el.innerText).toContain("SUBMIT");
 });

 //test for checking load html section on page or not
 it('should show subscription section', () => {
  fixture.detectChanges();
  const el = fixture.debugElement.query(By.css('.add-paste')).nativeElement;
  expect(el).toBeTruthy();
});

//test for title
it('should show title', () => {
  fixture.detectChanges();
  const el = fixture.debugElement.query(By.css('ion-title')).nativeElement;
  expect(el.innerText).toContain("Page Two");
});


     //button is clicked or not , fuction is called or not
     it('should integrate function Buttonclick from component function()', () =>{
   
      spyOn(comp1, "buttonclick").and.callThrough();
      comp1.buttonclick();
      expect(comp1.buttonclick).toHaveBeenCalled();
      });

 //integration testing for pipes
 it('providing 3 values to short and filter', () => {
  expect(pipe.transform('',"quetion,answer","").toBeTruthy);
});


//
it('providing 3 values to short and filter', () => {
  spyOn(pipe,'transform').and.callThrough;
  pipe.transform('',"quetion,answer","");
  expect(pipe.transform).toHaveBeenCalled();
});
});