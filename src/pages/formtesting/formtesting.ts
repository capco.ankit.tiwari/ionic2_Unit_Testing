import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-formtesting',
  templateUrl: 'formtesting.html'
})
export class FormtestingPage {
  form: FormGroup;
  public formData={email:"",
  password:123,
value:false};
  constructor(public navCtrl: NavController, private fb: FormBuilder) {
   
  }

  ngOnInit() { 
    this.form = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.pattern("[^ @]*@[^ @]*")]],
      password: ['', [
        Validators.required,
        Validators.minLength(8),
        Validators.pattern(/[0-9]/)]],

        setvalue:['',  Validators.required],
    });
  }
login(){
  console.log("ded");
}
}
