import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormtestingPage } from './FormTesting';
import { IonicModule, Platform, NavController} from 'ionic-angular/index';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
 
describe('FormTesting', () => {
  let de: DebugElement;
  let comp: FormtestingPage;
  let fixture: ComponentFixture<FormtestingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormtestingPage],
      imports: [
        IonicModule.forRoot(FormtestingPage),
        ReactiveFormsModule, FormsModule
      ],
      providers: [
        NavController
      ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormtestingPage);
    comp = fixture.componentInstance;

     // get test component from the fixture
    
     comp.ngOnInit();
  
  });
// Form Testing on Validation and Patterns
  it('should create component', () => expect(comp).toBeDefined());

  
  //form empty validation
  it('form invalid when empty', () => {
    expect(comp.form.valid).toBeFalsy();
  });

  //email validation
  it('email field validity', () => {
    let errors = {};
    let email = comp.form.controls['email'];
    expect(email.valid).toBeFalsy();

    // Email field is required
    errors = email.errors || {};
    expect(errors['required']).toBeTruthy();

    // Set email to something
    email.setValue("test");
    errors = email.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['pattern']).toBeTruthy();

    // Set email to something correct
    email.setValue("test@example.com");
    errors = email.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['pattern']).toBeFalsy();
  });


  //password validation
  it('password field validity', () => {
    let errors = {};
    let password = comp.form.controls['password'];

    // password field is required
    errors = password.errors || {};
    expect(errors['required']).toBeTruthy();
   
     // password is required
     errors = password.errors || {};
     expect(errors['required']).toBeTruthy();
    
     // Set password to something
    password.setValue("123456");
    errors = password.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['minlength']).toBeTruthy();
    expect(errors['pattern']).toBeFalsy();

    // Set password to something correct
    password.setValue("12345678");
    errors = password.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['minlength']).toBeFalsy();
    expect(errors['pattern']).toBeFalsy();
  });

  
  it('email should be ok', async(() => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      let input = fixture.debugElement.query(By.css('#email'));
      let el = input.nativeElement;

      expect(el.value).toBe('');

      el.value = 'someValue';
      fixture.detectChanges();
      expect(el.value).not.toBe('');
      // expect(fixture.componentInstance.formData.email).not.toBe('');
    });
  }));

  it('password should be ok', async(() => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      let input = fixture.debugElement.query(By.css('#password'));
      let el = input.nativeElement;
      el.value = 123;
      fixture.detectChanges();
      // expect(comp.formData.password).toMatch(/\[0-9]/g);

      expect(el.value).not.toBe('');
     
     // expect(el.value).toBe(123);
      // expect(fixture.componentInstance.formData.email).not.toBe('');
    });
  }));

  it('checkbox is checked if value is true', async(() => {
    comp.formData.value = true;
    fixture.detectChanges();
  
    fixture.whenStable().then(() => {
      const inEl = fixture.debugElement.query(By.css('#simpleInput'));
      expect(inEl.nativeElement.checked).toBe(true);
    });
  }));
  


});
