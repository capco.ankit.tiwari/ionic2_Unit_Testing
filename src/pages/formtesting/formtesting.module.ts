import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormtestingPage } from './formtesting';

@NgModule({
  declarations: [
    FormtestingPage,
  ],
  imports: [
    IonicPageModule.forChild(FormtestingPage),
  ],
})
export class FormtestingPageModule {}
