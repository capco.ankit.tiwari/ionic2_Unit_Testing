import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { Page1 } from './page1';
import { IonicModule, Platform, NavController, NavPush} from 'ionic-angular/index';
import { FaqMocks ,NavMock } from '../../../test-config/mocks-ionic';
import { Page2 } from '../page2/page2';
import { PipesModule } from '../../pipes/pipes.module';



describe('Page1', () => {
  let de: DebugElement;
  let el: HTMLElement;
  let comp: Page1;
  let fixture: ComponentFixture<Page1>;




  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Page1],
      imports: [
        IonicModule.forRoot(Page1),
        PipesModule
      ],
      providers: [
        {
            provide: NavController,
            useClass: NavMock,
            
        }
    ]

    });
  }));

  beforeEach(() => {
    
    fixture = TestBed.createComponent(Page1);
    comp = fixture.componentInstance;
    de = fixture.debugElement.query(By.css('h3'));
  });


  //To check the page is created or not
  it('should create component', () => expect(comp).toBeDefined());

  // For Html tags like h1 , p etc
  it('should have expected <h3> text', () => {
    fixture.detectChanges();
    const h3 = de.nativeElement;
    expect(h3.innerText).toMatch(/ionic/i,
      '<h3> should say something about "Ionic"');
  });

  // for images 
  it('should show the favicon as <img>', () => {
    fixture.detectChanges();
    const img: HTMLImageElement = fixture.debugElement.query(By.css('img')).nativeElement;
    expect(img.src).toContain('assets/icon/favicon.ico');
  });

 // ionviewdid load is loaded or not 
  it('should call function ionViewDidLoad()', () =>{
   
    spyOn(comp, "ionViewDidLoad").and.callThrough();
    comp.ionViewDidLoad();
    expect(comp.ionViewDidLoad).toBeDefined;

    });

    //button is clicked or not , fuction is called or not
    it('should call function Buttonclick function()', () =>{
   
      spyOn(comp, "buttonFunction").and.callThrough();
      comp.buttonFunction();
      expect(comp.buttonFunction).toHaveBeenCalled();
      });

      // navCtrl.push method is called or not to push the page , navigate to next page
      it('should be able to launch  page2', () => {
 
        let navCtrl = fixture.debugElement.injector.get(NavController);
        spyOn(navCtrl, 'push');
 
        de = fixture.debugElement.query(By.css('button'));
 
        de.triggerEventHandler('click', Page2);
        expect(navCtrl.push).toHaveBeenCalled;
       
 
    });

     //button is clicked or not , fuction is called or not
     it('should call function showArrayDATA function()', () =>{
   
      spyOn(comp, "showArrayDATA").and.callThrough();
      comp.showArrayDATA();
      expect(comp.showArrayDATA).toHaveBeenCalled();
      });

    // Json data array should not be empty

    it('should return a non empty array', () => {
 
      let result = comp.showArrayDATA();

      expect(Array.isArray(result)).toBeTruthy;
      expect(result.length).toBeGreaterThan(0);
  }
);

      // Json object should have quetion and answer parameters in it
      it('Json object  should have quetion and answer parameters in its body ', () => {
 
        let result = comp.showArrayDATA();
        let firstFaq =result[0];
    
      
        expect(firstFaq.question).toBeTruthy;
        expect(firstFaq.answer).toBeTruthy;
        expect(firstFaq.timestamp).toBeTruthy;
       
    }
    );
          // H3 and P tag should display the json object data
          it(' H3 and P tag should display the json object data ', () => {
      
            let result = comp.showArrayDATA();
            let firstFaq =result[0];
        
            fixture.detectChanges();
 
            de = fixture.debugElement.query(By.css('ion-item h3'));
            el = de.nativeElement;
     
            expect(el.textContent).toContain(firstFaq.question);
                    
        }
        );

        // change detection in front end show and hide 
      it(' Toggal testing to check toggal show and hide the data ', () => {
      
        let result = comp.show;
      
       spyOn(comp, "showArrayDATA").and.callThrough();
       comp.showArrayDATA();
       fixture.detectChanges();
       expect(result).toBeTruthy;

       spyOn(comp,'show').and.callThrough();
       comp.showArrayDATA();
       fixture.detectChanges();
       expect(result).toBeFalsy;
                
    }
    );
    

   
});
