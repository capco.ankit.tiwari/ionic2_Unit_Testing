import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Page1 } from './page1';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    Page1,
  ],
  imports: [
    IonicPageModule.forChild(Page1),
    PipesModule
  ],
})
export class Page1Module {}
