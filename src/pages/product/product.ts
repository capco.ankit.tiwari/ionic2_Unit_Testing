import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Products } from '../../providers/products/products'
 
@Component({
  selector: 'page-product',
  templateUrl: 'product.html'
})
export class ProductPage {
 //calle:any;
  constructor(public navCtrl: NavController, public productsService: Products) {
    
  }
 
  ionViewDidLoad() {
 this.productsService.load();
  }
 
}