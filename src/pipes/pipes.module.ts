import { NgModule } from '@angular/core';
import { ShortArrayPipe } from './short-array/short-array'
@NgModule({
	declarations: [ShortArrayPipe],
	imports: [],
	exports: [ShortArrayPipe]
})
export class PipesModule {}
