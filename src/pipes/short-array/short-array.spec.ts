import { TestBed, async } from '@angular/core/testing';
import { ShortArrayPipe } from './short-array'

describe('Pipe: Sort -array ', () => {
    let pipe = new ShortArrayPipe();
  it('create an instance', () => {
   
    expect(pipe).toBeTruthy();
  });

  it('providing 3 values to short and filter', () => {
    spyOn(pipe,'transform').and.callThrough;
    pipe.transform('',"quetion,answer","");
    expect(pipe.transform).toHaveBeenCalled();
  });

  // it('providing 3 values to short and filter', () => {
  //   expect(pipe.transform("ANKIT")).toEqual("ankit");
  // });

});