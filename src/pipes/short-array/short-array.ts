import { Pipe, PipeTransform } from "@angular/core";

/**
 * Generated class for the SortArrayPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: "shortArray"
})
export class ShortArrayPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value, keys: string,term:string) {
    if (!term) return value;
    return (value || []).filter((item) => keys.split(',').some(key => item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key])));
  }
//  data:String = "ANKIT";
//   transform(value:String){
//    if(!value){
//      return value;
//    }else{
//      return value.toLowerCase;
//    }
//   }

}
